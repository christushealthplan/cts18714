/* global TimelineMax, Power4, EB, EBG */

// Broadcast Events shim
// ====================================================================================================
(function() {
    if (typeof window.CustomEvent === 'function') { return false; }

    function CustomEvent(event, params) {
        params = params || { bubbles: false, cancelable: false, detail: undefined };
        var evt = document.createEvent('CustomEvent');
        evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail);
        return evt;
    }

    CustomEvent.prototype = window.Event.prototype;
    window.CustomEvent = CustomEvent;
})();

// Timeline
// ====================================================================================================
var timeline = (function MasterTimeline() {

    var tl;
    var win = window;

    function doClickTag() { window.open(window.clickTag); }

    function initTimeline() {
        document.querySelector('#ad .banner').style.display = 'block';
        document.getElementById('ad').addEventListener('click', doClickTag);
        createTimeline();
    }

    function createTimeline() {
        tl = new TimelineMax({delay: 0.25, onStart: updateStart, onComplete: updateComplete, onUpdate: updateStats});
        // ---------------------------------------------------------------------------

        tl.add('frame1')
        .from('.bottom', 1, {y:"15%", ease: Power3.easeInOut }, 'frame1')
        .from('.hl1', 1.5, {opacity: 0, ease: Power3.easeInOut }, 'frame1+=0.5')
        .from('.logo1', 1.5, {opacity: 0, ease: Power3.easeInOut }, 'frame1+=1')
        .to('.bottom', 1.5, {opacity: 0, ease: Power3.easeInOut }, 'frame1+=2.5')
        .to('.hl1', 1.25, {opacity: 0, ease: Power3.easeInOut }, 'frame1+=2.5')
        .to('.logo1', 1.25, {opacity: 0, ease: Power3.easeInOut }, 'frame1+=2.5')

        tl.add('frame2')
        .from('.image', 1.5, {opacity: 0, ease: Power3.easeInOut }, 'frame2')
        .from('.logo2', 1.5, {opacity: 0, ease: Power3.easeInOut }, 'frame2')
        .from('.hl2', 2, {x:"-15%", ease: Power3.easeOut }, 'frame2+=1')
        .from('.hl2', 2, {opacity: 0, ease: Power3.easeOut }, 'frame2+=1')
        .to('.hl2', 1.25, {opacity: 0, ease: Power3.easeOut }, 'frame2+=3')
        .from('.hl3a', 2, {x:"-15%", ease: Power3.easeOut }, 'frame2+=4')
        .from('.hl3a', 2, {opacity: 0, ease: Power3.easeOut }, 'frame2+=4')
        .from('.hl3b', 2, {x:"-15%", ease: Power3.easeOut }, 'frame2+=4.5')
        .from('.hl3b', 2, {opacity: 0, ease: Power3.easeOut }, 'frame2+=4.5')
        .to('.hl3a', 1.25, {opacity: 0, ease: Power3.easeInOut }, 'frame2+=6.5')
        .to('.hl3b', 1.25, {opacity: 0, ease: Power3.easeInOut }, 'frame2+=6.5')
        .to('.logo2', 1.25, {opacity: 0, ease: Power3.easeInOut }, 'frame2+=6.5')
        .to('.image', 1.25, {opacity: 0, ease: Power3.easeInOut }, 'frame2+=6.5')

        tl.add('frame3')
        .from('.hl4a', 2, {opacity: 0, ease: Power3.easeInOut }, 'frame3')
        .from('.hl4b', 2, {opacity: 0, ease: Power3.easeInOut }, 'frame3+=0.5')
        .from('.logo3', 2, {opacity: 0, ease: Power3.easeInOut }, 'frame3+=1')


        
        // ---------------------------------------------------------------------------

        // DEBUG:
        // tl.play('frame3'); // start playing at label:frame3
        // tl.pause('frame3'); // pause the timeline at label:frame3
    }

    function updateStart() {
        var start = new CustomEvent('start', {
            'detail': { 'hasStarted': true }
        });
        win.dispatchEvent(start);
    }

    function updateComplete() {
        var complete = new CustomEvent('complete', {
            'detail': { 'hasStopped': true }
        });
        win.dispatchEvent(complete);
    }

    function updateStats() {
        var statistics = new CustomEvent('stats', {
            'detail': { 'totalTime': tl.totalTime(), 'totalProgress': tl.totalProgress(), 'totalDuration': tl.totalDuration()
            }
        });
        win.dispatchEvent(statistics);
    }

    function getTimeline() {
        return tl;
    }

    return {
        init: initTimeline,
        get: getTimeline
    };

})();

// Banner Init
// ====================================================================================================
timeline.init();
